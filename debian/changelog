python-envisage (4.9.0-3) UNRELEASED; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 15 Jan 2020 14:13:17 +0000

python-envisage (4.9.0-2) unstable; urgency=medium

  * Team upload.
  * Add envisage.egg-info/SOURCES.txt to debian/clean, to fix build twice
    in a row (closes: #671324).

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 30 Dec 2019 11:11:48 +0300

python-envisage (4.9.0-1) unstable; urgency=medium

  * Team Upload.

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * Remove debian/pycompat, it's not used by any modern Python helper
  * Convert git repository from git-dpm to gbp layout

  [ Scott Talbert ]
  * Remove outdated orig-tar.sh
  * Update to latest upstream release 4.9.0
  * Switch from Python 2 to Python 3 (Closes: #937730)
  * Update and convert d/copyright to DEP5 format
  * Use secure URL in d/watch

 -- Scott Talbert <swt@techie.net>  Thu, 26 Dec 2019 10:56:49 -0500

python-envisage (4.4.0-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Varun Hiremath ]
  * New upstream release
  * Bump Standards-Version to 3.9.5

 -- Varun Hiremath <varun@debian.org>  Sat, 15 Mar 2014 23:41:54 -0400

python-envisage (4.1.0-2) unstable; urgency=low

  * Fix typo in Breaks and Replaces (Closes: #668459)
  * Add Suggests: python-apptools, python-chaco (Closes: #668460)
  * Fix Vcs-{Svn, Browser} URLs

 -- Varun Hiremath <varun@debian.org>  Tue, 24 Apr 2012 19:08:44 -0400

python-envisage (4.1.0-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.9.3

 -- Varun Hiremath <varun@debian.org>  Mon, 23 Apr 2012 15:56:40 -0400

python-envisage (4.0.0-1) unstable; urgency=low

  * New upstream release
  * Rename package to python-envisage - replaces python-envisagecore
    and python-envisageplugins packages
  * Update d/control, d/copyright and d/watch

 -- Varun Hiremath <varun@debian.org>  Sat, 09 Jul 2011 00:05:01 -0400

python-envisagecore (3.2.0-2) unstable; urgency=low

  * Team upload.
  * Rebuild to pick up 2.7 and drop 2.5 from supported versions
  * d/control: Bump Standards-Version to 3.9.2

 -- Sandro Tosi <morph@debian.org>  Sat, 07 May 2011 14:47:58 +0200

python-envisagecore (3.2.0-1) unstable; urgency=low

  * New upstream release
  * Convert to dh_python2 (Closes: #617004)

 -- Varun Hiremath <varun@debian.org>  Tue, 05 Apr 2011 23:54:28 -0400

python-envisagecore (3.1.3-1) experimental; urgency=low

  * New upstream release
  * d/control: Bump Standards-Version to 3.9.1

 -- Varun Hiremath <varun@debian.org>  Sun, 17 Oct 2010 21:33:28 -0400

python-envisagecore (3.1.2-1) unstable; urgency=low

  * New upstream release
  * Switch to source format 3.0
  * Bump Standards-Version to 3.8.4

 -- Varun Hiremath <varun@debian.org>  Sun, 28 Feb 2010 14:35:30 -0500

python-envisagecore (3.1.1-2) unstable; urgency=low

  * debian/control:
    - Add python-pkg-resources to Depends (Closes: #560724)
    - Bump Standards-Version to 3.8.3

 -- Varun Hiremath <varun@debian.org>  Tue, 15 Dec 2009 10:47:32 -0500

python-envisagecore (3.1.1-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.8.2

 -- Varun Hiremath <varun@debian.org>  Mon, 20 Jul 2009 03:15:07 -0400

python-envisagecore (3.1.0-1) unstable; urgency=low

  * New upstream release

 -- Varun Hiremath <varun@debian.org>  Thu, 26 Mar 2009 20:03:16 -0400

python-envisagecore (3.0.1-1) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Varun Hiremath ]
  * New upstream release
  * debian/control: add python-setupdocs to Build-Depends

 -- Varun Hiremath <varun@debian.org>  Sun, 28 Dec 2008 22:38:20 -0500

python-envisagecore (3.0.0-1) experimental; urgency=low

  * Initial release

 -- Varun Hiremath <varun@debian.org>  Sun, 26 Oct 2008 01:36:05 -0400
